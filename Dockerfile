FROM node:14.19.2-alpine
# Create app directory
WORKDIR /app
# Install app dependencies
COPY package*.json ./
RUN npm i
# Bundle app source
COPY . .
EXPOSE 3000
CMD [ "node", "node.js" ]
# Stage 2 - the production environment
# FROM nginx:stable-alpine
# COPY --from=builder /app /usr/share/nginx/html
# COPY --from=builder /app/nginx.conf /etc/nginx/conf.d/default.conf

