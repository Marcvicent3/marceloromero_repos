const express = require('express');
const app = express();
const port = 3000;
const Sequelize = require('sequelize');
require('dotenv').config();


const sequelize = new Sequelize(process.env.MYSQL_DATABASE, process.env.MYSQL_USER, process.env.MYSQL_PASSWORD, {
    host: process.env.MYSQL_HOST,
    port: process.env.MYSQL_PORT,
    dialect: 'mysql'
});


const Calculation = sequelize.define('db_calculation', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    num1: {
        type: Sequelize.FLOAT,
        allowNull: false
    },
    num2: {
        type: Sequelize.FLOAT,
        allowNull: false
    },
    operation: {
        type: Sequelize.STRING,
        allowNull: false
    },
    result: {
        type: Sequelize.FLOAT,
        allowNull: false
    }
});


app.use(express.json());

app.post('/sum', async (req, res) => {
    const { num1, num2 } = req.body;
    const result = num1 + num2;

    const calculation = await Calculation.create({
        num1,
        num2,
        operation: 'sum',
        result
    });

    res.json({ result });
});

app.post('/subtract', async (req, res) => {
    const { num1, num2 } = req.body;
    const result = num1 - num2;

    const calculation = await Calculation.create({
        num1,
        num2,
        operation: 'subtract',
        result
    });

    res.json({ result });
});

app.post('/multiply', async (req, res) => {
    const { num1, num2 } = req.body;
    const result = num1 * num2;

    const calculation = await Calculation.create({
        num1,
        num2,
        operation: 'multiply',
        result
    });

    res.json({ result });
});

app.post('/divide', async (req, res) => {
    const { num1, num2 } = req.body;
    if (num2 === 0) {
        res.status(400).json({ error: 'Cannot divide by 0' });
    } else {
        const result = num1 / num2;

        const calculation = await Calculation.create({
            num1,
            num2,
            operation: 'divide',
            result
        });

        res.json({ result });
    }
});

sequelize.sync().then(() => {
    app.listen(port, () => {
        console.log(`Calculator API listening at http://localhost:${port}`);
    });
});

module.exports = app;