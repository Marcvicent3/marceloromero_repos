const request = require('supertest');
const app = require('../node');

describe('Calculator REST API', () => {
    it('should add two numbers', (done) => {
        request(app)
            .post('/sum')
            .send({ num1: 5, num2: 10 })
            .expect(200)
            .expect((res) => {
                res.body.result.should.equal(15);
            })
            .end(done);
    });

    it('should subtract two numbers', (done) => {
        request(app)
            .post('/subtract')
            .send({ num1: 5, num2: 10 })
            .expect(200)
            .expect((res) => {
                res.body.result.should.equal(-5);
            })
            .end(done);
    });

    it('should multiply two numbers', (done) => {
        request(app)
            .post('/multiply')
            .send({ num1: 5, num2: 10 })
            .expect(200)
            .expect((res) => {
                res.body.result.should.equal(50);
            })
            .end(done);
    });

    it('should divide two numbers', (done) => {
        request(app)
            .post('/divide')
            .send({ num1: 50, num2: 10 })
            .expect(200)
            .expect((res) => {
                res.body.result.should.equal(5);
            })
            .end(done);
    });
});