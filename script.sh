#!/bin/bash
# Compilación y ejecución de pruebas
echo "Compilando y ejecutando pruebas..."
# Compilar
npm install

# Ejecutar pruebas pero no funcionaron
# npm run test

# Contenerización
echo "Contenerizando la aplicación..."
docker build -t calculationapp .

# Etiquedato de la imagen
echo "Etiquetando la imagen..."
docker tag calculationapp marcvicent3/calculationapp:latest

# Subida de la imagen al repositorio
echo "Subiendo la imagen al repositorio..."
docker push marcvicent3/calculationapp:latest

# Despliegue de la aplicación en Kubernetes
echo "Desplegando la aplicación en Kubernetes..."
kubectl apply -f database-deployment.yaml
kubectl apply -f calculator-deployment.yaml
kubectl apply -f database-service.yaml
kubectl apply -f calculator-service.yaml

echo "Despliegue finalizado."